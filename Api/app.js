const express = require('express')
const multer = require('multer');
const cors = require('cors')
const fs = require('fs');
const TelegramBot = require('node-telegram-bot-api');
const bodyParser = require('body-parser')
const axios = require('axios');

const app = express()
const port = 5000

const botToken = '6157376622:AAHguN7hFojL3vAaFX5hN0QTBPoH3PjlmaQ';
const apiUrl = 'https://api.telegram.org/bot6157376622:AAHguN7hFojL3vAaFX5hN0QTBPoH3PjlmaQ/setWebhook';
const webhookUrl = 'https://62fb-103-233-64-27.in.ngrok.io/webhook';
const allowedUpdates = ['my_chat_member','message','channel_post'];

const bot = new TelegramBot(botToken);
const chatIds = [];

app.use(cors());
app.use(bodyParser.json())

axios.post(apiUrl, { url: webhookUrl, allowed_updates: allowedUpdates })
  .then(response => {
    console.log(`Response status: ${response.status}`);
    console.log(`Response data: ${JSON.stringify(response.data)}`);
  })
  .catch(error => {
    console.error(error);
  });

// bot.setWebHook({
//     url: 'https://62fb-103-233-64-27.in.ngrok.io/webhook'
// }).then(() => {
//     console.log('Webhook set up');
// }).catch((error) => {
//     console.error('Error setting up webhook:', error);
// });

const upload = multer({
    dest: 'uploads/',
    fileFilter: function (req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            return cb(new Error('Only image files are allowed!'));
        }
        cb(null, true);
    }
});

bot.on('my_chat_member', (my_chat_member) => {
    if (my_chat_member.new_chat_member.status === 'kicked' || my_chat_member.new_chat_member.status === 'left') {
        var chatId = my_chat_member.chat.id;
        var index = chatIds.indexOf(chatId);
        if (index > -1) {
            chatIds.splice(index, 1);
        }
    }
    else if (my_chat_member.new_chat_member.status === 'administrator') {
        var chatId = my_chat_member.chat.id;
        chatIds.push(chatId);
    }
    console.log('chatids', chatIds)
});

app.post('/webhook', (req, res) => {
    const update = req.body;
    console.log('update',update)
    bot.processUpdate(update);
    res.sendStatus(200);
});


app.post('/api/upload', upload.single('image'), (req, res) => {

    console.log(req.file);
    const path = req.file.path;
    const caption = `<strong>${req.body.heading}</strong>

<i>${req.body.summery}</i>

${req.body.content}
<a href="https://www.hashd.news/the-crypto-community-reacts-violently-to-the-solana-outage">Visit HashD</a>
`
    chatIds.forEach((chatId) => {
        console.log('------------------------------------------------------------------current_chat',chatId)
        bot.sendMessage(chatId, caption, {
            parse_mode: 'HTML',
        })
            .then(() => {
                console.log('Message sent successfully to chat ID:', chatId);
            })
            .catch((error) => {
                console.error('Error sending message to chat ID:', chatId, error);
            });
    });
    console.log('chatIds',chatIds)
    res.json({ success: true });
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))